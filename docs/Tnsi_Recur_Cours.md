# <center> RÉCURSIVITÉ </center>


## Introduction

<img src='https://capytale2.ac-paris.fr/web/sites/default/files/2021-08-25-20-53-02//ot_91e7bdd3-7ced-47e5-bac4-6203f778e616/inception.jpg' style='float:right;' width='400' alt="photo_abyme">

On va aborder le concept de **récursivité**.
* Une fonction récursive est une fonction qui fait appel à elle même dans sa définition.
* On peut rapprocher le concept de récursivité à celui de récurrence en mathématiques.
* Ce style de programmation permet de résoudre certains problèmes qu'il n'est parfois pas facile de traiter en programmant uniquement avec des boucles.

Ci-contre : Une image tirée du film "Inception"(Christopher Nolan, 2010).

## 1. Une autre façon de voir les choses

!!! exo 

    Pour définir la somme des $n$ premiers entiers, on à l'habitude d'écrire la formule suivante : $0+1+2+...+n$. A l'aide d'une boucle `for`, compléter la fonction `somme(n)` ci-dessous.


    ```python
    def somme(n):
        '''Renvoie la somme des n premiers entiers
        parametre : n entier naturel
        return : la somme des n premiers entiers
        '''
        S=0
        
        
        
        return S

    assert(somme(3)==6)
    assert(somme(5)==15)
    ```

    
#### Remarque :
* Cette fonction fait bien ce que l'on attend d'elle. Mais on peut remarquer que ce code n'est pas directement lié à la définition. Il n'y a rien qui laisse supposer qu'une variable intermédiaire `S` est nécessaire pour calculer cette somme.


### Définition récursive 
* Il existe une autre façon de faire : pour calculer la somme des $n$ premiers entiers, il suffit d'ajouter $n$ à la somme des $n-1$ premiers entiers !
* Il s'agit d'une définition mathématique que l'on peut rapprocher de la notion de récurrence pour les suites.
* La fonction somme peut être définie ainsi :

<center> 
    $somme(n)=\left\{ \begin{array}{ll} 0 \;si \; n=0\\
    n+somme(n-1) \;si\; n>0 \end{array} \right.$ 
</center>

Exemples :
* $somme(0)=0$
* $somme(1)=1+somme(0)=1+0=1$
* $somme(2)=2+somme(1)=2+1=3$
* $somme(3)=3+somme(2)=3+3=6$

#### Remarques :
* La définition de $somme(n)$ dépend donc de $somme(n-1)$.
* La fonction somme fait appel à elle-même dans sa définition.Il s'agit d'une définition récursive.
* Ainsi , pour connaître $somme(n)$, il faut connaître $somme(n-1)$, donc $somme(n-2)$ et ce jusqu'à $somme(0)$ qui vaut $0$.
* On obtient $somme(n)$ en ajoutant toutes ces valeurs.


### Programmation récursive
L'intérêt de cette définition récursive de la fonction somme est qu'elle est calculable, c'est à dire exécutable par un ordinateur.

En particulier, voici le code python de cette définition :


```python
def somme(n):
    if n==0:
        return 0
    else:
        return n + somme(n-1)
    
assert(somme(3)==6)
assert(somme(0)==0)
```

<img src='https://capytale2.ac-paris.fr/web/sites/default/files/2021-08-25-20-53-02//ot_91e7bdd3-7ced-47e5-bac4-6203f778e616/somme_n.png' style='float:right;' width='300'>

#### Remarque : Comment se passe l’exécution de ce programme ?

* Si `n`vaut `0`, alors la valeur `0` est renvoyée, sinon on renvoie `n + somme(n-1)`.


* Cet appel qui fait référence à la fonction que l'on est en train de définir est un appel récursif.


* Voici ci-contre une façon de représenter ce qui se passe lorsque `somme(3)` est appelé.


* On appelle cette représentation un arbre d'appels.


* Pour calculer la valeur renvoyée par `somme(3)`, il faut appeler `somme(2)` qui va appeler `somme(1)` qui va appeler `somme(0)` qui va renvoyer `0`.


* Le calcul se fait donc à rebours : le calcul de `somme(0)=0` permet celui de `somme(1)=1+0` puis celui de `somme(2)=2+1` et enfin celui de `somme(3)=3+3`.


### A retenir

Une **fonction récursive** est une fonction qui fait appel à elle même dans sa définition.

Une formulation récursive d'une fonction est constituée de plusieurs cas:
* Le ou les **cas de base** : Ce sont ceux pour lesquels on peut obtenir le résultat sans faire appel à la fonction définie elle-même. 
* Le ou les **cas récursifs** : Ce sont ceux qui renvoient à la fonction en train d'être définie. 


Dans l'exemple de la fonction `somme(n)`, le cas de base est `n=0`.Les cas de base sont habituellement les cas de valeurs particulières faciles à obtenir.

Pour la fonction `somme(n)`, le cas récursif est `n > 0` .

Remarques :
* Pour écrire une fonction récursive, on peut commencer par identifier les cas de base.
* Il faut faire confiance à la récursion : on ne doit pas chercher à comprendre ce qui va se passer exactement lors des appels et supposer qu'ils vont donner de bons résultats pour les valeurs sur lesquelles ils opèrent.


En python, une fonction récursive simple s'écrit :

```python
def fctRecur(arguments):
    if condition d'arret:
        return valeur
    appel recursif
```



##### Exercice 2 : 
En mathématiques, la **factorielle** d'un entier naturel $n$ est le produit des nombres entiers strictement positifs inférieurs ou égaux à $n$, on la note $n!$ .

Par exemples, $4!=24$ et $5!=120$.

Compléter la fonction ci-dessous en utilisant la récursivité.


```python
def factor(n):
    '''parametre : n entier naturel non nul
    return : la factorielle de n'''
    
    
    pass
    
 
assert(factor(4)==24)
assert(factor(5)==120)
```


### Exercice 2
On considère les deux fonctions suivantes :


```python
def bottom_up(n):
    if n == 0:
        pass
    else:
        bottom_up(n-1)
        print(n)

def top_down(n):
    if n == 0:
        pass
    else:
        print(n)
        top_down(n-1)
```

Que vont produire bottom_up(5) et top_down(5) ?


##### Exercice 3 :
1. Compléter la défintion récursive ci-dessous de la fonction $puissance(x,n)$ qui calcule la valeur de la puissance nième d'un réel $x$:  $x^n=\underbrace{{x} \times {x} \times {...} \times {x}}_{n \; fois}$

<center> 
    $puissance(x,n)=\left\{ \begin{array}{ll} ? \;si \; n=0\\
? \;si\; n>0 \end{array} \right.$ 
</center>

2. Compléter ci-dessous la fonction `puissance(x,n)` à l'aide de la définition précédente.

```python
def puissance(x,n):
    '''Calcule la puissance nième de x
    paramètres : 
    x de type int ou float
    n entier naturel
    
    return :
    la puissance nième de x, de type int ou float
    '''
    pass
    
assert(puissance(2,0)==1)
assert(puissance(3,3)==27)
```

